<?php

/**
 * @file
 * uw_ct_web_form.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_web_form_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_web_form_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_web_form_node_info() {
  $items = array(
    'uw_web_form' => array(
      'name' => t('Web form'),
      'base' => 'node_content',
      'description' => t('Similar to the web page content type, but with web form capabilities'),
      'has_title' => '1',
      'title_label' => t('Heading'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
