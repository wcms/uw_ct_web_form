<?php

/**
 * @file
 * uw_ct_web_form.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_web_form_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access all webform results'.
  $permissions['access all webform results'] = array(
    'name' => 'access all webform results',
    'roles' => array(
      'administrator' => 'administrator',
      'form results access' => 'form results access',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform results'.
  $permissions['access own webform results'] = array(
    'name' => 'access own webform results',
    'roles' => array(
      'administrator' => 'administrator',
      'form results access' => 'form results access',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform submissions'.
  $permissions['access own webform submissions'] = array(
    'name' => 'access own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'add users to webform results access'.
  $permissions['add users to webform results access'] = array(
    'name' => 'add users to webform results access',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform_access_granular',
  );

  // Exported permission: 'administer honeypot'.
  $permissions['administer honeypot'] = array(
    'name' => 'administer honeypot',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'honeypot',
  );

  // Exported permission: 'administer pdfs'.
  $permissions['administer pdfs'] = array(
    'name' => 'administer pdfs',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'fillpdf',
  );

  // Exported permission: 'bypass granular form results access'.
  $permissions['bypass granular form results access'] = array(
    'name' => 'bypass granular form results access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_access_granular',
  );

  // Exported permission: 'bypass honeypot protection'.
  $permissions['bypass honeypot protection'] = array(
    'name' => 'bypass honeypot protection',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'honeypot',
  );

  // Exported permission: 'create uw_web_form content'.
  $permissions['create uw_web_form content'] = array(
    'name' => 'create uw_web_form content',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create webform content'.
  $permissions['create webform content'] = array(
    'name' => 'create webform content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete all webform submissions'.
  $permissions['delete all webform submissions'] = array(
    'name' => 'delete all webform submissions',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'delete any uw_web_form content'.
  $permissions['delete any uw_web_form content'] = array(
    'name' => 'delete any uw_web_form content',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any webform content'.
  $permissions['delete any webform content'] = array(
    'name' => 'delete any webform content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_web_form content'.
  $permissions['delete own uw_web_form content'] = array(
    'name' => 'delete own uw_web_form content',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own webform content'.
  $permissions['delete own webform content'] = array(
    'name' => 'delete own webform content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own webform submissions'.
  $permissions['delete own webform submissions'] = array(
    'name' => 'delete own webform submissions',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit all webform submissions'.
  $permissions['edit all webform submissions'] = array(
    'name' => 'edit all webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit any uw_web_form content'.
  $permissions['edit any uw_web_form content'] = array(
    'name' => 'edit any uw_web_form content',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any webform content'.
  $permissions['edit any webform content'] = array(
    'name' => 'edit any webform content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_web_form content'.
  $permissions['edit own uw_web_form content'] = array(
    'name' => 'edit own uw_web_form content',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own webform content'.
  $permissions['edit own webform content'] = array(
    'name' => 'edit own webform content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own webform submissions'.
  $permissions['edit own webform submissions'] = array(
    'name' => 'edit own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit webform components'.
  $permissions['edit webform components'] = array(
    'name' => 'edit webform components',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'enter uw_web_form revision log entry'.
  $permissions['enter uw_web_form revision log entry'] = array(
    'name' => 'enter uw_web_form revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter webform revision log entry'.
  $permissions['enter webform revision log entry'] = array(
    'name' => 'enter webform revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form authored by option'.
  $permissions['override uw_web_form authored by option'] = array(
    'name' => 'override uw_web_form authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form authored on option'.
  $permissions['override uw_web_form authored on option'] = array(
    'name' => 'override uw_web_form authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form promote to front page option'.
  $permissions['override uw_web_form promote to front page option'] = array(
    'name' => 'override uw_web_form promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form published option'.
  $permissions['override uw_web_form published option'] = array(
    'name' => 'override uw_web_form published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form revision option'.
  $permissions['override uw_web_form revision option'] = array(
    'name' => 'override uw_web_form revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_web_form sticky option'.
  $permissions['override uw_web_form sticky option'] = array(
    'name' => 'override uw_web_form sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'form editor' => 'form editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform authored by option'.
  $permissions['override webform authored by option'] = array(
    'name' => 'override webform authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform authored on option'.
  $permissions['override webform authored on option'] = array(
    'name' => 'override webform authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform promote to front page option'.
  $permissions['override webform promote to front page option'] = array(
    'name' => 'override webform promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform published option'.
  $permissions['override webform published option'] = array(
    'name' => 'override webform published option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform revision option'.
  $permissions['override webform revision option'] = array(
    'name' => 'override webform revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override webform sticky option'.
  $permissions['override webform sticky option'] = array(
    'name' => 'override webform sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'publish all pdfs'.
  $permissions['publish all pdfs'] = array(
    'name' => 'publish all pdfs',
    'roles' => array(
      'administrator' => 'administrator',
      'form editor' => 'form editor',
    ),
    'module' => 'fillpdf',
  );

  // Exported permission: 'publish own pdfs'.
  $permissions['publish own pdfs'] = array(
    'name' => 'publish own pdfs',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'fillpdf',
  );

  return $permissions;
}
